package com.example.dietcontrolv10;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLOutput;

public class MainActivity extends AppCompatActivity {

    //Declaring the required Variables

    private byte counter = 0; //For counting the Junk Food Intake
    private String logdTag;   //For Debugging Purpose
    private TextView junkCounter;
    private TextView popMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // linking TextView with java from resource folder

        junkCounter = findViewById(R.id.JunkCounter);
        popMessage = findViewById(R.id.PopMessage);

    }

    // Working of the + Plus Button / Increment Button and Secret Message prompt

    public void counterIncrement(View view) {

        counter += 1; //Increasing the counter by 1
        junkCounter.setText(String.valueOf(counter)); // Changing the TextView value with Counter Value


        // Secret Message Section

        popMessage.setVisibility(TextView.INVISIBLE); // Setting Text View Value to Invisible

        // Used Switch to show when the secret message should pop and when it should be invisible

        switch (counter) {
            case 5:
                popMessage.setVisibility(TextView.VISIBLE);
                popMessage.setText(R.string.popMessage1);
                break;

            case 6:

            case 11:
                popMessage.setVisibility(TextView.INVISIBLE);
                break;

            case 10:
                popMessage.setVisibility(TextView.VISIBLE);
                popMessage.setText(R.string.popMessage2);
                break;

            case 15:
                popMessage.setVisibility(TextView.VISIBLE);
                popMessage.setText(R.string.popMessage3);
                break;

        }

        //  Log.d(logdTag, "counterIncrement: Increased By +1 " + counter);  // Uncomment for Debugging Purpose

    }

    // Function for Resetting the value of the counter


    public void counterSetZero(View view) {

        counter = 0; // Setting the counter value to Zero
        junkCounter.setText(String.valueOf(counter));

      //  Log.d(logdTag, "counterSetZero: Setting Counter to Zero " + counter); // Uncomment for Debugging Purpose

    }

    // Function For Toast

    public void messagePop(View view) {
        Toast.makeText(getApplicationContext(), "Wanna get a slim " +
                "look ? Then Start Dieting with Us !", Toast.LENGTH_SHORT).show();

    }

    // End Of My code

    //More code can be Added Below


}

